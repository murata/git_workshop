\documentclass[aspectratio=169, 9pt]{beamer}

\usepackage{template}

\newcommand{\hilight}[1]{\colorbox{green}{#1}}

\definecolor{RoyalBlue}{rgb}{0.25,0.41,0.88}
\def\Emph{\textcolor{RoyalBlue}}

\mode<presentation>
{
  \usetheme{Copenhagen}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{beaver} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{caption}[numbered]
  \useoutertheme[subsection=false]{miniframes}
} 

\usefonttheme[onlymath]{serif}
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    % \centering{\insertshorttitle{}}
    \insertframenumber/\inserttotalframenumber
}
\setbeamerfont{footline}{series=\bfseries}
\setbeamercovered{transparent}
\setbeamertemplate{itemize item}[circle]
\setbeamertemplate{itemize subitem}[triangle]
\setbeamertemplate{itemize subsubitem}[square]


\title[Version Control]{Version Control and its Applications in Academia}

\author{Murat Ambarkutuk}

\institute{
    \scriptsize
	\vspace*{0.2cm}
    Computer Engineering, The Bradley Department of Electrical and Computer Engineering, Virginia Tech\\
    \vspace*{0.5cm}
    Virginia Tech's Smart Infrastructure Laboratory, Virginia Tech\\
}

\date{\today}

\begin{document}
%=======================================================%
\begin{frame}
  \titlepage
\end{frame}  

\begin{frame}{Credits}
    This workshop is inspried by and adapted from different work:
    \begin{itemize}
        \item Dr. Kulumani's work entitled ``Git to Work!''
        \item Nick Quaranto's ``what git is not'' article\footnote{\footnotesize http://gitready.com/beginner/2009/02/19/what-git-is-not.html}
        \item Roman Jordan's ``Git it Right'' article published on Linux Journal\footnote{https://www.linux-magazine.com/Issues/2018/216/Version-Control-with-Git}
    \end{itemize}
\end{frame}

\begin{frame}{Outline}
    We will cover three concepts:
    \begin{enumerate}
        \item Version control (5 mins.)
        \item Git (15 mins.)
        \item Hands-on Practice (30 mins.) 
    \end{enumerate}
\end{frame}

\section{Version Control}
\begin{frame}{}
    \begin{figure}
        \centering
        \includegraphics[width=0.5\linewidth]{figures/Folder_Pizza.png}
        \caption{We all know this mess. We are not foreign to it. Let's own it and try to change something in our workflow.}
    \end{figure}
\end{frame}

\begin{frame}{What is version control?}

\begin{block}{Version Control}
    A system to record or manage changes to files or sets of files over time.
\end{block}

\pause
\begin{columns}[T]
    \column{0.5\textwidth}
    Benefits: \\
    \begin{itemize}
        \item<1-> \Emph{Collaboration} - Teams generate many variations 
        \item<2-> \Emph{Branching} - Bugs may only exist in specific versions
        \item<3-> \Emph{Trunk} - Make changes without causing more errors
    \end{itemize}
    \column{0.5\textwidth}
    \only<4>{
        Use cases: \\
        \begin{itemize}
            \item Code
            \item Resume, Personal Websites
            \item Manuscript Drafting, Paper writing (\LaTeX)
        \end{itemize}
    }
\end{columns}
\end{frame}  

\begin{frame}{Version Control Software}
    \begin{columns}
\begin{column}[t]{0.5\textwidth}
\begin{itemize}
    \item Centralized Model: CVS, SVN, etc.
\end{itemize}

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{figures/centralized.png}        
\end{figure}
\end{column}

\begin{column}[t]{0.5\textwidth}
\begin{itemize}
    \item Distributed Model: Mercurial, Git, etc.
\end{itemize}
\begin{figure}
    \centering
    \includegraphics[height=0.6\textheight]{figures/distributed.png}
\end{figure}

\end{column}
\end{columns}

\end{frame}

\section{Git}
\begin{frame}{What is Git?}
\begin{figure}
    \includegraphics[width=0.3\textheight]{figures/Git-logo.pdf}
\end{figure}

\begin{columns}[t]
    \begin{column}{0.3\textwidth}
        What is not Git?
        \begin{itemize}
            \item Magic
            \item Difficult to learn
            \item Difficult to use
            \item Github, Gitlab, etc.
        \end{itemize}
    \end{column} 
    \begin{column}{0.65\textwidth}
        What is Git?
        \begin{itemize}
            \item Piece of software to track changes
            \item A very large number of resources to learn from
            \item Jargon makes it seem scary
            \item Works locally
        \end{itemize}
    \end{column} 
\end{columns}
\end{frame}

\begin{frame}{How does Git work?}
\begin{itemize}
    \item<1-> Track changes in \Emph{TEXT} files!
    \item<2-> Git stores \Emph{Snapshots} - Saving the current state!
    \item<3-> Everything is local - no internet needed
    \item<4-> Integrity - Use of hash functions
\end{itemize}

\begin{figure}
    \centering
    \includegraphics<2>[width=0.75\textwidth]{figures/deltas.png}
    \includegraphics<3->[width=0.75\textwidth]{figures/snapshots.png}
\end{figure}

\end{frame}

\begin{frame}{Simple Workflow}

\begin{enumerate}
    \item Modify files in your diretory
    \item Stage the files by adding snapshots of the current state
    \item Commit and permanently store the snapshot to the Git repo
\end{enumerate}


\begin{figure}
    \centering
    \includegraphics<1>[height=0.5\textheight]{lifecycle}
\end{figure}
\end{frame}

\begin{frame}{Git Terminology}
    \begin{itemize}
    \item Repo - Project folder that contains all the files
    \item Commit - ``Revision'' - unique change/version of a file/files
    \item Branch - Parallel version of a repo
    \item Remote - Copy of repo that lives on another computer
    \item Clone - Create a copy from a remote
    \item Push - Send your changes to a remote
    \item Fetch - Retrieve the changes from the remote
    \item Merge - Combine changes between branches
    \item Pull - Fetch and Merge changes at once
\end{itemize}
\end{frame}



\section{Resources and Tips}
\begin{frame}{Some helpful tips!}
    \begin{columns}[t]
    \begin{column}{0.3\textwidth}
        Additional Resources:
        \begin{enumerate}
            \item Cheatsheets
            \item \href{https://git-scm.com/}{Git Book}
            \item \href{http://marklodato.github.io/visual-git-guide/index-en.html}{Visual Git}
            \item \href{http://stackoverflow.com/questions/315911/git-for-beginners-the-definitive-practical-guide}{StackOverflow}
            \item Many available online
        \end{enumerate}    
    \end{column}
    \begin{column}{0.65\textwidth}
        Some helpful tips:
        \begin{itemize}
            \item Not the only/best solution!
            \item For \LaTeX: every sentence on a separate line
            \item Don't put your repo in Google Drive/Dropbox
            \item Commit often with \Emph{USEFUL} messages!
            \item If you get lost... GUI: \texttt{gitk}, \texttt{gitkraken}, or others
            \item use gitignore.io for generation of .gitignore file
        \end{itemize}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}{Hands-on Section}
    \centering
    \LARGE Let's try it out!
\end{frame}
\end{document}

